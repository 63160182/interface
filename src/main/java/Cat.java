/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Cat extends LandAnimal{
    private String name;

    public Cat(String name) {
        super(name, 4);
    }
    

    @Override
    public void run() {
        System.out.println("Cat: "+name+" run");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: "+name+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Cat: "+name+" eat");
    }

    
    @Override
    public void speak() {
        System.out.println("Cat: "+name+" speak");
    }

    
}
