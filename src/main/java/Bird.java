/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Bird extends Poultry{
    private String name;

    public Bird(String name) {
        super(name,2);
    }
    
    @Override
    public void fly() {
        System.out.println("Bird: "+name+" fly");
    }

    @Override
    public void sleep() {
        System.out.println("Bird: "+name+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Bird: "+name+" eat");
    }

    
    @Override
    public void speak() {
        System.out.println("Bird: "+name+" speak");
    }
    
}
