/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Snake extends Reptile{
    private String name;

    public Snake(String name) {
        super(name, 0);
    }
    
    @Override
    public void crawl() {
        System.out.println("Snake: "+name+" sleep");
    }

    @Override
    public void sleep() {
        System.out.println("Snake: "+name+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Snake: "+name+" eat");
    }

    @Override
    public void speak() {
        System.out.println("Snake: "+name+" speak");
    }
    
}
